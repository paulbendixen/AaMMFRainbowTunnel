#include <user_config.h>
#include <SmingCore.h>

#include <Adafruit_NeoPixel/Adafruit_NeoPixel.h>
#include <SPI.h>
#include <MFRC522/MFRC522.h>
#include "NeoPixelColour.hpp"

#include <pinAssignments.hpp>

template< typename T, size_t N >
constexpr size_t arrayLength( T (&arr)[N] )
{
	return N;
}
#ifndef WIFI_SSID
#define WIFI_SSID "XXX" // Put you SSID and Password here
#define WIFI_PWD "XXX"
#endif

// Which pin on the Esp8266 is connected to the NeoPixels?
// First try used Pin15 = D8, Same side as 5v Pin2 = D4
// For SPI MOSI use PIN13 as output
//constexpr int PIN = 15;
constexpr byte PIN = D1Mini::D4;
constexpr byte SS_Pin = D1Mini::D8;
constexpr byte RESET_Pin = D1Mini::D3;

// How many NeoPixels are attached to the Esp8266?
constexpr int NUMPIXELS = 150;
constexpr int bandLength = 4;
constexpr int RAINBOWS = 20;

Adafruit_NeoPixel strip = Adafruit_NeoPixel(NUMPIXELS, PIN, NEO_GRB + NEO_KHZ800);
MFRC522 rfidReader(SS_Pin, RESET_Pin );

void StartDemo(void);

Timer ColorWipeTimer;
Timer ReaderTimer;


constexpr NeoPixel::Colour fromHSV( uint16_t hue, uint8_t saturation, uint8_t value )
{
	return NeoPixel::Colour{ };
}

NeoPixel::Colour rainbow[] = {
		NeoPixel::Colour{ 0x0f, 0x00, 0x00 },
		NeoPixel::Colour{ 0x0f, 0x06, 0x00 },
		NeoPixel::Colour{ 0x0f, 0x0e, 0x00 },
		NeoPixel::Colour{ 0x00, 0x0f, 0x00 },
		NeoPixel::Colour{ 0x00, 0x0f, 0x0f },
		NeoPixel::Colour{ 0x00, 0x00, 0x0f },
		NeoPixel::Colour{ 0x09, 0x00, 0x0f }
		};

uint8_t strengths[arrayLength(rainbow)] = { 9, 9, 9, 9, 9, 9, 9 };

void setPixelColor( Adafruit_NeoPixel& neopixels, int pixel, NeoPixel::Colour col )
{
	neopixels.setPixelColor( pixel, col.red, col.green, col.blue );
}
void RainbowDash()
{
	static int offset = 0;
	for ( int pixel = 0; pixel < NUMPIXELS; pixel++ )
	{
		auto colourIdentifier = ( ( offset + pixel ) / bandLength ) % arrayLength(rainbow);
		auto col = rainbow[ colourIdentifier ] * strengths[ colourIdentifier ];
		setPixelColor( strip, pixel, col );
	}
	strip.show();
	offset++;
	if ( offset >= RAINBOWS * bandLength * arrayLength( rainbow ) )
	{
		offset = 0;
		Serial.print( "Setting new strengths : [ " );
		for ( auto& s: strengths )
		{
			if ( s > 0 )
			{
				s--;
			}
			Serial.printf( "%d ", s );
		}
		Serial.print( "]\n" );
	}
}

void ReadRFID()
{
	if (rfidReader.PICC_IsNewCardPresent() )
		if( rfidReader.PICC_ReadCardSerial() )
		{
			auto serial = rfidReader.uid;
			switch ( serial.uidByte[2] )
			{
				case 0xb5:
					strengths[ 0 ] = 9;
					break;
				case 0xa4:
					strengths[ 1 ] = 9;
					break;
				case 0xa3:
					strengths[ 2 ] = 9;
					break;
				case 0xd5:
					strengths[ 3 ] = 9;
					break;
				case 0x93:
					strengths[ 4 ] = 9;
					break;
				case 0x82:
					strengths[ 5 ] = 9;
					break;
				case 0x83:
					strengths[ 6 ] = 9;
					break;
				case 0xbb:
					for ( auto &s :strengths )
					{
						s = 9;
					}
					break;
				default:
					Serial.printf( "Unknown card 0x%x\n", serial.uidByte[ 2 ] );
			}
			/*
			Serial.printf( "Read Id of %d bytes", serial.size );
			for (byte serialByte = 0; serialByte < serial.size; serialByte++ )
			{
				Serial.printf( " 0x%x", serial.uidByte[serialByte]);
			}
			Serial.print( '\n' );
			 */
		}
		else
		{
			Serial.print( "New Card, but can't read it\n");
		}
}

void got_IP(IPAddress ip, IPAddress netmask, IPAddress gateway)
{
	Serial.printf("IP: %s\n", ip.toString().c_str());
	//You can put here other job like web,tcp etc.
}

// Will be called when WiFi station loses connection
void connect_Fail(String SSID, uint8_t ssid_len, uint8_t bssid[6], uint8_t reason)
{
	Serial.println("I'm NOT CONNECTED!");
}

void init()
{
	Serial.begin(SERIAL_BAUD_RATE);  // 115200 by default
	Serial.systemDebugOutput(true); // Disable debug output to serial
	Serial.print( "We're live\n");


	// Wifi could be used eg. for switching Neopixel from internet
	// could be also dissabled if no needed

	/*
	WifiStation.config(WIFI_SSID, WIFI_PWD);
	WifiStation.enable(true);
	WifiAccessPoint.enable(false);
	WifiEvents.onStationDisconnect(connect_Fail);
	WifiEvents.onStationGotIP(got_IP);
	*/

	strip.begin(); //init port
	ColorWipeTimer.initializeMs( 50, RainbowDash ).start( true );

	SPI.begin();
	rfidReader.PCD_Init();
	ReaderTimer.initializeMs( 50, ReadRFID ).start( true );
}
