//
// Created by expert on 02/10/2019.
//

#ifndef BASIC_NEOPIXEL_PINASSIGNMENTS_HPP
#define BASIC_NEOPIXEL_PINASSIGNMENTS_HPP


namespace D1Mini {
	constexpr byte D0 = 16;
	constexpr byte D1 = 5;
	constexpr byte D2 = 4;
	constexpr byte D3 = 0;
	constexpr byte D4 = 2;
	constexpr byte D5 = 14;
	constexpr byte D6 = 12;
	constexpr byte D7 = 13;
	constexpr byte D8 = 15;
};
#endif //BASIC_NEOPIXEL_PINASSIGNMENTS_HPP
