//
// Created by expert on 09/09/2019.
//

#ifndef BASIC_NEOPIXEL_NEOPIXELSPI_HPP
#define BASIC_NEOPIXEL_NEOPIXELSPI_HPP
#include <NeoPixelColour.hpp>
#include <cstddef>


template <size_t LEDS, size_t COLOURS = 3 >
class NeoPixelSpi
{
	private:
		NeoPixel::NeoPixelColour< COLOURS > pixels[ LEDS ];
};


#endif //BASIC_NEOPIXEL_NEOPIXELSPI_HPP
