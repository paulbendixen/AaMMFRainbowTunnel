//
// Created by expert on 09/09/2019.
//

#include <algorithm>
#include "NeoPixelColour.hpp"

NeoPixel::Colour::Colour( uint8_t red, uint8_t green, uint8_t blue )
{
	this->red = red;
	this->blue = blue;
	this->green = green;
}

NeoPixel::Colour NeoPixel::Colour::operator/( uint8_t scale )
{
	Colour retval = *this;
	retval.red /= scale;
	retval.green /= scale;
	retval.blue /= scale;
	return retval;
}

NeoPixel::Colour& NeoPixel::Colour::dim( NeoPixel::Colour dimFrom )
{
	auto fraction = dimFrom / 32;
	red -= fraction.red;
	green -= fraction.green;
	blue -= fraction.blue;
	return *this;
}

NeoPixel::Colour NeoPixel::Colour::operator*( uint8_t scale )
{
	Colour retval = *this;
	retval.red *= scale;
	retval.green *= scale;
	retval.blue *= scale;
	return retval;
}
