//
// Created by expert on 09/09/2019.
//

#ifndef BASIC_NEOPIXEL_NEOPIXELCOLOUR_HPP
#define BASIC_NEOPIXEL_NEOPIXELCOLOUR_HPP
#include <cstdint>
#include <cstddef>

namespace NeoPixel
{
struct Colour
{
	uint8_t red = 0;
	uint8_t green = 0;
	uint8_t blue = 0;

	Colour() = default;
	Colour( uint8_t red, uint8_t green, uint8_t blue );
	Colour operator/( uint8_t scale );
	Colour operator*( uint8_t scale );
	Colour& dim( Colour dimFrom );

};

template< size_t COLOURS = 3 >
struct NeoPixelColour
{

	uint8_t data[COLOURS][4];
};

template< size_t COLOURS>
NeoPixelColour< COLOURS > transformColour( const Colour& col )
{
	NeoPixelColour< COLOURS > retval;
	//for ( retval.data[])
	return retval;
};
}

#endif //BASIC_NEOPIXEL_NEOPIXELCOLOUR_HPP
